import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

@DatabaseTable
class User {
    @DatabaseField (generatedId = true)
    private int id;
    @DatabaseField
    private String username;
    @DatabaseField
    private String serializedFP;

    private static Dao<User, String> userDao;

    private static final String URL = "jdbc:sqlite:user.db";

    static {
        try {
            ConnectionSource connectionSource = new JdbcConnectionSource(URL);
            userDao = DaoManager.createDao(connectionSource, User.class);
            TableUtils.createTableIfNotExists(connectionSource, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    int getId() {
        return id;
    }

    String getUsername() {
        return username;
    }

    String getSerializedFP() {
        return serializedFP;
    }

    void setId(int id) {
        this.id = id;
    }

    void setUsername(String username) {
        this.username = username;
    }

    void setSerializedFP(String serializedFP) {
        this.serializedFP = serializedFP;
    }

    static Dao<User, String> getUserDao () {
        return userDao;
    }

    void save () throws SQLException {
        userDao.createIfNotExists(this);
    }
}
