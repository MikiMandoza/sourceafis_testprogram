import com.zkteco.biometric.FingerprintSensorErrorCode;
import com.zkteco.biometric.FingerprintSensorEx;

import javax.swing.*;
import java.awt.*;

public class View extends JFrame {
    private JPanel panel1;
    private JTextField textFieldUserName;
    private JButton registerButton;
    private JPanel pannelImage;
    private JLabel infoLabel;
    private JButton openButton;
    private JButton closeButton;
    private JPanel panelFingerPrintImage;
    private JLabel labelMatch;
    private JTabbedPane mainPane;
    private JButton imageButton;

    View(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | UnsupportedLookAndFeelException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        imageButton.setPreferredSize(new Dimension(400, 400));

        setContentPane(mainPane);
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    JFrame getFrame () {
        return this;
    }

    JLabel getInfoLabel () {
        return this.infoLabel;
    }

    JButton getImageButton () {
        return imageButton;
    }

    JButton getOpenButton () {
        return this.openButton;
    }

    JButton getRegisterButton() {
        return registerButton;
    }

    JButton getCloseButton() {
        return closeButton;
    }

    public static void main (String[] args){
        View j = new View();
    }

    public String getUserName() {
        return textFieldUserName.getText();
    }
}
